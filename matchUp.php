<?php
include "top.php";
?>
<?php
//initialize vars
//missing the check box var which is $country[]
$user1='';
$user2 ='';
$pass1 = '';
$pass2 = '';
$tier = 'high';

//to popuate the checkboxes for countries
$query1 = 'SELECT * FROM `tblLeagues` GROUP BY fldCountryName';
$countries = $thisDatabaseReader->select($query1, "", 0, 0, 0, 0, false, false);

//when the sybmit button is pressed
if (isset($_POST["btnSubmit"])) {
    //collect info from form
    $user1 = htmlentities($_POST["txtuser1"], ENT_QUOTES, "UTF-8");
    $user2 = htmlentities($_POST["txtuser2"], ENT_QUOTES, "UTF-8");
    $pass1 = htmlentities($_POST["txtpass1"], ENT_QUOTES, "UTF-8");
    $pass2 = htmlentities($_POST["txtpass2"], ENT_QUOTES, "UTF-8");
    $tier = htmlentities($_POST["radTier"], ENT_QUOTES, "UTF-8");
    
    //bellow is used for checking if the user name exists in the database, if not it tells the to create one
    //also checks that the username entered matches their password
    $sameName = 1;
    if($user1 == $user2){
        $sameName=0;
    }
    $user1Test = 0;
    $user2Test = 0;
    $pass1Test = 0;
    $pass2Test = 0;
    $q = "SELECT pmkUser, fldPass FROM tblUsers";
    $usernames = $thisDatabaseReader->select($q, "", 0, 0, 0, 0, false, false);
    foreach($usernames as $username){
        if($user1 == $username['pmkUser']){
            $user1Test = 1;
            if($pass1 == $username['fldPass']){
                $pass1Test = 1;
            }
        }
        if($user2 == $username['pmkUser']){
            $user2Test = 1;
            if($pass2 == $username['fldPass']){
                $pass2Test = 1;
            }
        }
    }
    
    
    //get check box choices into $checkedCounties which is an array,
    //$checkedCountiries always containes 29 values all true or false depeneding if they were checked or not
    $query1 = 'SELECT * FROM `tblLeagues` GROUP BY fldCountryName';
    $countries = $thisDatabaseReader->select($query1, "", 0, 0, 0, 0, false, false);
    $checkedCountries = array();
    //makes sure at least one country has been selected
    if(isset($_POST["country"])){
        foreach($countries as $country){
            $b = 'false';
            foreach ($_POST["country"] as $aCountry){
                if($country['fldCountryName']==$aCountry){
                    $b = 'true';
                }
            }
            array_push($checkedCountries, $b);
        }
    }
//    print '<pre>';
//    print_r($checkedCountries) ;
//    print '</pre>';
    
    //this if statement checks that the form is filled out properly
    //if so insert everything into proper tables and go to game.php
    if ($sameName !=0 && $user1Test !=0 && $user2Test !=0 && $pass1Test !=0 && $pass2Test !=0 && $user1 !='' && $user2 !='' && isset($_POST["country"])){
        
        $query1 = 'INSERT INTO tblCountries (fldARG, fldAUS, fldAUT, fldBEL, fldBRA, fldCHI, fldCOL, fldDEN, fldENG, fldFRA, fldGER, fldIRE, fldITA, 
            fldJAP, fldKOR, fldMEX, fldNED, fldNOR, fldPOL, fldPOR, fldROW, fldRUS, fldSAU, fldSCO, fldSPA, fldSWE, fldSWI, fldTUR, fldUSA) VALUES 
            ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);';  
            
        $insertGuy1 = $thisDatabaseWriter->insert($query1, $checkedCountries, 0, 0, 0, 0, false, true);
        
        $selectIdQuery = 'SELECT `pmkCountriesId` FROM `tblCountries` ORDER BY `pmkCountriesId` DESC';
        $lastId = $thisDatabaseReader->select($selectIdQuery, "", 0, 1, 0, 0, false, false);
        
        $query = "INSERT INTO tblMatchUps SET
          fnkUser1 ='". $user1 . "',
          fnkUser2 = '" . $user2 ."',
          fnkCountriesId = '". $lastId[0]['pmkCountriesId'] . "',
          fldTier = '" . $tier . "' ;";

        $insertGuy = $thisDatabaseWriter->insert($query, "", 0, 0, 8, 0, false, true);
        
        $selectIdQuery1 = 'SELECT `pmkMatchId` FROM `tblMatchUps` ORDER BY `pmkMatchId` DESC';
        $lastId1 = $thisDatabaseReader->select($selectIdQuery1, "", 0, 1, 0, 0, false, false);
        
        //this match ups id as a string
        $id = (string) $lastId1[0]['pmkMatchId'];
        
        //this cookie will last one day because of the 1 at the end
        setcookie("matchId" ,$id,time()+(60*60*24*1),'/');
        
        //this will send the user to the specified page
        header("Location: game.php");
        exit;
    }
    //all the if statements bellow are just to display to the user what their errors are
    if($sameName == 0){
        print "<h4> You cannot play yourself. </h4>" ;
    }
    if($user1Test == 0 || $user1 ==''){
        print "<h4> User 1 does not have an account. <a href='createAccount.php'>Create one here.</a> </h4>" ;
    }
    elseif($pass1Test == 0){
        print "<h4> User 1's password is incorrect. </h4>" ;
    }
    if($user2Test == 0 || $user2 == ''){
        print "<h4> User 2 does not have an account. <a href='createAccount.php'>Create one here.</a></h4>" ;
    }
    elseif($pass2Test == 0){
        print "<h4> User 2's password is incorrect. </h4>" ;
    }
    if(!isset($_POST["country"])){
        print "<h4> Must select at least one country. </h4>" ;
    }
    
}//ends if set
        ?>
<form action="matchUp.php"
          method="POST"
    id="frmRegister">
              
    <fieldset class="info">
        <legend>Create a Match Up!</legend>
        <fieldset class = "guy">
            <legend>User 1</legend>
                    <label for="txtuser1" class="required">User 1 Email
                        <input type="text" 
                               id="txtuser1" 
                               name="txtuser1"
                               value="<?php print $user1; ?>"
                               tabindex="90" 
                               maxlength="45" 
                               placeholder="Enter email for user one"
                               onfocus="this.select()" 
                               >
                    </label>
                    <label for="txtpass1" class="required">User 1 Password
                        <input type="PASSWORD" 
                               id="txtpass1" 
                               name="txtpass1"
                               value="<?php print $pass1; ?>"
                               tabindex="91" 
                               maxlength="45" 
                               placeholder="Enter password for user one"
                               onfocus="this.select()" 
                               >
                    </label>
        </fieldset>
        
        <fieldset class = "guy">
            <h1>VS.</h1>
        </fieldset>
        
        <fieldset class = "guy">
            <legend>User 2</legend>
                    <label for="txtuser2" class="required">User 2 Email 
                           <input type="text" 
                                  id="txtuser2" name="txtuser2"
                                  value="<?php print $user2; ?>"
                                  tabindex="110" 
                                  maxlength="45" 
                                  placeholder="Enter email for user two"
                                  onfocus="this.select()"
                                  autofocus>
                    </label>
                    <label for="txtpass2" class="required">User 2 Password
                        <input type="PASSWORD" 
                               id="txtpass2" 
                               name="txtpass2"
                               value="<?php print $pass2; ?>"
                               tabindex="111" 
                               maxlength="45" 
                               placeholder="Enter password for user two"
                               onfocus="this.select()" 
                               >
                    </label>
    </fieldset>   
</fieldset> 
    
<script>
$( '#container .toggle-button' ).click( function () {
    $( '#container input[type="checkbox"]' ).prop('checked', this.checked)
  })
</script>    
    
    

    <fieldset class="checkbox">
        <legend>Select Countries for Your Team's to be Drawn From.</legend>
    
<script language="JavaScript">
function toggle(source) {
  checkboxes = document.getElementsByName('country[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
  
}
</script>

<input type="checkbox" onClick="toggle(this)" /> Toggle All Countries<br/>
<?php
    print '<ul>';
    foreach($countries as $country){
    print '<li><label><input type="checkbox" 
                  id="chkCountry" 
                  name="country[]" 
                  value="' . $country['fldCountryName'] . '"
                  tabindex="420"> ' . $country['fldCountryName'] . ' </label></li>';
    }
    print '</ul>' ;
    ?>
</fieldset>
                
                
<fieldset class="radio">
    <legend>How good do you want your teams to be?</legend>
    <ul>
        <li><label><input type="radio" 
                  id="radTier" 
                  name="radTier" 
                  value="high"
                  <?php if ($tier == "high") print 'checked' ?>
                  tabindex="330">The Best</label></li>
        <li><label><input type="radio" 
                  id="radTier" 
                  name="radTier" 
                  value="med"
                  <?php if ($tier == "med") print 'checked' ?>
                  tabindex="340">Average</label></li>
        <li><label><input type="radio" 
                  id="radTier" 
                  name="radTier" 
                  value="low"
                  <?php if ($tier == "low") print 'checked' ?>
                  tabindex="350">The Worst</label></li>
    </ul>
</fieldset> 
    
    
    
            <fieldset class="buttons">
                <legend></legend>
                <input type="submit" class="btnSubmit" name="btnSubmit" value="Start Game" tabindex="900" class="button">
            </fieldset> <!-- ends buttons -->
    
</form>

<?php include "footer.php"; ?>

</body>
</html>