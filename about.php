<?php
include "top.php";
?>
<h1>More About Us</h1>
<h2>Alex Ram</h2>
<!--I found the code for this here : http://stackoverflow.com/questions/5198392/css-float-floating-an-image-to-the-left-of-the-text and used the css because I liked it. -->
<div class="post-container">                
    <div class="post-thumb">
       <figure class = "photo">
        <img src="realalex.jpg">
        <figcaption>
            Alex enjoying the snow
        </figcaption>
        </figure> 
    </div>
    <div class="post-content">
        <p>Alex is an experienced coder, who held the role of Lead Developer and Web Designer on this project. "From this project I became more comfortable with all my favorite languages for web design, including CSS, PHP, SQL and Javascript. I also learned a lot about Git work flow and how to work with a group in an organized fashion. I enjoyed working on this project and would love to do something like this again."  </p>
   </div>
</div>


<h2>Felix Madero</h2>
<div class="post-container">                
    <div class="post-thumb">
       <figure class = "photo">
        <img src="felix.jpg">
        <figcaption>Felix on a boat</figcaption>
        </figure> 
    </div>
    <div class="post-content">
        <p>Felix is Computer Science and Information Systems major at UVM and a big soccer/FIFA fan. He mainly worked on back end features of the website and learned a lot about SQL, PHP, and databases through this project.</p>

   </div>
</div>

<h2>Jacob Normyle</h2>
<div class="post-container">                
    <div class="post-thumb">
       <figure class = "photo">
        <img src="jdog.jpg">
        <figcaption>J-dog</figcaption>
        </figure> 
    </div>
    <div class="post-content">
        <p>BSCS major I like guitar, video games and ice cream.</p>
   </div>
</div>
<?php include "footer.php"; ?>

</body>
</html>