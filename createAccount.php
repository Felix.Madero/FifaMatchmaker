<?php
include "top.php";
?>
<?php
$user='';
$pass='';
$pass1='';
$firstName='';
$lastName='';
$skill = '';

if (isset($_POST["btnSubmit"])) { //this will be where you insert stuff 
$user = htmlentities($_POST["txtuser"], ENT_QUOTES, "UTF-8");
$pass = htmlentities($_POST["txtpass"], ENT_QUOTES, "UTF-8");
$pass1 = htmlentities($_POST["txtpass1"], ENT_QUOTES, "UTF-8");
$firstName = htmlentities($_POST["txtfirstName"], ENT_QUOTES, "UTF-8");
$lastName = htmlentities($_POST["txtlastName"], ENT_QUOTES, "UTF-8");
$skill = htmlentities($_POST["lstskill"], ENT_QUOTES, "UTF-8");

    $userTest = 0;
    $q = "SELECT pmkUser FROM tblUsers";
    $usernames = $thisDatabaseReader->select($q, "", 0, 0, 0, 0, false, false);
    foreach($usernames as $username){
        if($user == $username['pmkUser']){
            $userTest = 1;
        }
    }
    $passTest = 0;
    if ($pass == $pass1){
        $passTest =1;
    }
    
    
if ($user !="" && $pass !="" && $firstName !="" && $lastName !="" && $userTest != 1 && filter_var($user, FILTER_VALIDATE_EMAIL) && $passTest == 1){
$query = "INSERT INTO tblUsers SET
  pmkUser ='". $user . "',
  fldPass = '" . $pass ."',
  fldFirstName = '". $firstName . "',
  fldLastName = '" . $lastName . "',
  fldSkill = '" . $skill . "' ;";
    
$insertGuy = $thisDatabaseWriter->insert($query, "", 0, 0, 10, 0, false, true);

    //send the email, includes thank you message, a reminder of their username and password
    $msg = $firstName . ",\n\n"
            ."Thanks for creating an account with us.\n"
            ."In case you forget, here's your account information.\n"
            ."Username: " . $user . "\n"
            ."Password: " . $pass . "\n\n"
            . "Take it easy,\n"
            . "Alex, Felix & Jacob";    
    $msg = wordwrap($msg,70);
    mail($user,"Welcome to Fifa Match Maker",$msg);

    header("Location: index.php");
    exit;
}
if($userTest == 1){
    print "<h4> That username is being used. </h4>" ;
}
elseif(!filter_var($user, FILTER_VALIDATE_EMAIL) ) {
print "<h4> Enter a valid email. </h4>"   ;
}
if($pass =="") {
print "<h4> You forgot to enter a password. </h4>"   ;
}
if($firstName =="") {
print "<h4> You forgot to enter your FIRST name. </h4>"   ;
}
if($lastName =="") {
print "<h4> You forgot to enter your LAST name. </h4>"   ;
}
if($passTest == 0){
    print "<h4> Your passwords don't match. </h4>" ;
}
}
        ?>
<form action="createAccount.php"
          method="POST"
    id="frmRegister">
              
    <fieldset class="info">
        <div class ="center">
                    <legend>Create a new account!</legend>
                    
                    <label for="txtuser" class="required">Email address
                        <input type="text" 
                               id="txtuser" 
                               name="txtuser"
                               value="<?php print $user; ?>"
                               tabindex="90" 
                               maxlength="45" 
                               placeholder="Enter your email"
                               onfocus="this.select()" 
                               >
                    </label>
                    
                    <label for="txtpass" class="required">Password
                        <input type="password" 
                                  id="txtpass" name="txtpass"
                                  value="<?php print $pass; ?>"
                                  tabindex="110" 
                                  maxlength="45" 
                                  placeholder="Enter a password"
                                  onfocus="this.select()"
                                  autofocus>
                    </label> 
                    <label for="txtpass1" class="required">Verify Password
                        <input type="password" 
                                  id="txtpass1" name="txtpass1"
                                  value="<?php print $pass1; ?>"
                                  tabindex="111" 
                                  maxlength="45" 
                                  placeholder="Re-enter a password"
                                  onfocus="this.select()"
                                  autofocus>
                    </label> 
                        
                    <label for="txtfirstName" class="required">First Name
                        <input type="text" 
                               id="txtfirstName" 
                               name="txtfirstName"
                               value="<?php print $firstName; ?>"
                               tabindex="120" 
                               maxlength="45" 
                               placeholder="Enter first name"
                               onfocus="this.select()" 
                               >
                    </label>
                    <label for="txtlastName" class="required">Last Name
                        <input type="text" 
                               id="txtlastName" 
                               name="txtlastName"
                               value="<?php print $lastName; ?>"
                               tabindex="130" 
                               maxlength="45" 
                               placeholder="Enter last name"
                               onfocus="this.select()" 
                               >
                    </label>
                    <label for="lstskill">Choose your skill level</label>
                    <select id="lstskill" 
                            name="lstskill" 
                            tabindex="135" >
            <option <?php if ($skill == "Beginner") print " selected "; ?> value="Beginner">Beginner</option>
            <option <?php if ($skill == "Amateur") print " selected "; ?> value="Amateur">Amateur</option>
            <option <?php if ($skill == "Semi-Pro") print " selected "; ?> value="Semi-Pro">Semi-Pro</option>
            <option <?php if ($skill == "Professional") print " selected "; ?> value="Professional">Professional</option>
            <option <?php if ($skill == "World-Class") print " selected "; ?> value="World-Class">World-Class</option>
            <option <?php if ($skill == "Legendary") print " selected "; ?> value="Legendary">Legendary</option>
                    </select>
                    </div>
                
</fieldset> 
    
            <fieldset class="buttons">
                <legend></legend>
                <input type="submit" class="btnSubmit" name="btnSubmit" value="Create New User" tabindex="900" class="button">
            </fieldset> <!-- ends buttons -->
    
</form>

<?php include "footer.php"; ?>

</body>
</html>