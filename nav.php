<!-- ######################     Main Navigation   ########################## -->
<nav>
    <ol>
        <?php
        /* This sets the current page to not be a link. Repeat this if block for
         *  each menu item */
        if ($PATH_PARTS['filename'] == "index") {
            print '<li class="activePage">Home</li>';
        } else {
            print '<li><a href="index.php">Home</a></li>';
        }
        /* example of repeating */
        if ($PATH_PARTS['filename'] == "createAccount") {
            print '<li class="activePage">Create Account</li>';
        } else {
            print '<li><a href="createAccount.php"> Create Account</a></li>';
        }
        if ($PATH_PARTS['filename'] == "matchUp") {
            print '<li class="activePage">Match Up</li>';
        } else {
            print '<li><a href="matchUp.php">Match Up</a></li>';
        }
        if ($PATH_PARTS['filename'] == "game") {
            print '<li class="activePage">Game</li>';
        } else {
            print '<li><a href="game.php">Game</a></li>';
        }
        
        if ($PATH_PARTS['filename'] == "score") {
            print '<li class="activePage">LeaderBoard</li>';
        } else {
            print '<li><a href="score.php">LeaderBoard</a></li>';
        }
        if ($PATH_PARTS['filename'] == "about") {
            print '<li class="activePage">About Us</li>';
        } else {
            print '<li><a href="about.php">About Us</a></li>';
        }
        ?>
    </ol>
</nav>