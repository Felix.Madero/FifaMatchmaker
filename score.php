<?php
include "top.php";

$skill = '';
$selectedUser ='';
//query that loads the degrees into a listbox

//add number of users
//add number of games played
$whereCount = 0;
$data = "";

$query4 = 'SELECT COUNT(pmkUser) FROM tblUsers';
$totalPlayers = $thisDatabaseReader->select($query4, $data, 0, 0, 0, false, false);

$query5 = 'SELECT COUNT(pmkScoreId) FROM tblScores';
$totalGamesPlayed = $thisDatabaseReader->select($query5, $data, 0, 0, 0, false, false);

$query = 'SELECT fldUser1Score, fldUser2Score, fnkUser1, fnkUser2, fldUser1Team, fldUser2Team, fnkUser1Skill, fnkUser2Skill FROM tblScores WHERE fnkUser1 != "" ORDER BY pmkScoreId DESC Limit 20';
//Query which selects the scores based off of the chosen scoreId 
$scores = $thisDatabaseReader->select($query, $data, 1, 2, 2, false, false);
?>

<h1> LeaderBoard </h1>

<h3>Total Players: <?php print$totalPlayers[0][0] ?></h3>
<h3>Total Games Played: <?php print$totalGamesPlayed[0][0] ?></h3>
<!--The form below allows users to search for stats based on specific usernames        -->
<h2> Search for the Stats of a specific User </h2>

<div class="score">
    <form 
        method="POST"
        id="frmDefaultPlan">
        
            <legend>Search for a User</legend>
            <div class="post-thumb">
            <label for="txtUserName" class="search">User's Email:
                <input type="text" id="txtUserName" name="txtUserName"
                       value="<?php print $selectedUser; ?> "
                       tabindex="100" maxlength="45" placeholder="Enter a Username"
                       onfocus="this.select()"
                       >
            </label>
            </div>
            <div class="post-content">
            <fieldset class="buttons">
                <input type="submit" class="btnSubmit" name="btnUser" value="Select a User" tabindex="900" class="button">
            </fieldset> <!-- ends buttons -->
            </div>

    </form>

<?php
if (isset($_POST['btnUser'])) {
    $whereCount1 = 1;
    $selectedUser = htmlentities($_POST["txtUserName"], ENT_QUOTES, "UTF-8");
    $query3 = 'SELECT pmkUser, fldWins, fldLosses, fldTies, fldGoalsScored, fldGoalsLet, fldSkill FROM tblUsers WHERE pmkUser =' . '"' . $selectedUser . '"';
    $selectedUserStats = $thisDatabaseReader->select($query3, $data, $whereCount1, 0, 2, false, false);
    
    $userTest = 0;
    foreach($selectedUserStats as $username){
        if($selectedUser == $username['pmkUser']){
            $userTest = 1;
        }
    }
    if($userTest ==1){
    print '
    <table>
        <tr>
            <th>User</th>
            <th>Wins</th>
            <th>Losses</th>
            <th>Ties</th>
            <th>Goals Scored</th>
            <th>Goals Let</th>
            <th>Skill</th>

    </tr>';}
    
    if($userTest != 1){
        print '<h4> User does not exist! </h4>';
    }
    foreach($selectedUserStats as $userStat){
       print'<tr>
        <td>' . $userStat['pmkUser'] . '</td>
            <td>' . $userStat['fldWins'] . '</td>
                <td>' . $userStat['fldLosses'] . '</td>
                    <td>' . $userStat['fldTies'] . '</td>
                        <td>' . $userStat['fldGoalsScored'] . '</td>
                            <td>' . $userStat['fldGoalsLet'] . '</td>
                                <td>' . $userStat['fldSkill'] . '</td>
        
    </tr>';
    }
    if($userTest ==1){
        print'</table>';
    }
}
   
    ?>
    </div>

    <h2> Last 20 games played </h2>
    <div class ="score">
    <table>
        <tr>
            <th>Player 1</th>
            <th>Score</th>
            <th>User 1 Team</th>
            <th>User 1 Skill</th>
            <th>Player 2 </th>
            <th>Score</th>
            <th>User 2 Team</th>
            <th>User 2 Skill</th>

        </tr>
        <?php
        foreach ($scores as $score) {
            print '<tr>
        <td>' . $score['fnkUser1'] . '</td>
        <td>' . $score['fldUser1Score'] . '</td>
        <td>' . $score['fldUser1Team'] . '</td>
        <td>' . $score['fnkUser1Skill'] . '</td>
        <td>' . $score['fnkUser2'] . '</td>
        <td>' . $score['fldUser2Score'] . '</td>
        <td>' . $score['fldUser2Team'] . '</td>
        <td>' . $score['fnkUser2Skill'] . '</td>
    </tr>';
        }
        ?>
    </table>
        </div>
    
    <h2> View the Statistics of players of a specific skill level </h2>
    
    <?php
    if (isset($_POST['btnSubmit'])) {
        $skill = htmlentities($_POST["lstskill"], ENT_QUOTES, "UTF-8");
    }
        ?>
    
    <div class ="score">
    <form 
        method="POST"
        action ="score.php"
        id="frmDefaultPlan">
        <legend>Search by Skill</legend>
        <div class="post-thumb">
        <label for="lstskill">Choose a skill level</label>
        <select id="lstskill" 
                name="lstskill" 
                tabindex="135" >
            <option <?php if ($skill == "Beginner") print " selected "; ?> value="Beginner">Beginner</option>
            <option <?php if ($skill == "Amateur") print " selected "; ?> value="Amateur">Amateur</option>
            <option <?php if ($skill == "Semi-Pro") print " selected "; ?> value="Semi-Pro">Semi-Pro</option>
            <option <?php if ($skill == "Professional") print " selected "; ?> value="Professional">Professional</option>
            <option <?php if ($skill == "World-Class") print " selected "; ?> value="World-Class">World-Class</option>
            <option <?php if ($skill == "Legendary") print " selected "; ?> value="Legendary">Legendary</option>
    </select>
        </div>
        
        <div class="post-content">
    <fieldset class="buttons">
        <input type="submit" class="btnSubmit" name="btnSubmit" value="Select Skill level" tabindex="900" class="button">
    </fieldset> <!-- ends buttons -->
        </div>
</form>
        
    <?php
    if (isset($_POST['btnSubmit'])) {
        $skill = htmlentities($_POST["lstskill"], ENT_QUOTES, "UTF-8");
        $whereCount1 = 1;
        $query2 = 'SELECT pmkUser, fldWins, fldLosses, fldTies, fldGoalsScored, fldGoalsLet FROM tblUsers WHERE fldSkill =' . '"' . $skill . '"';
//Query which selects user stats based off the the selected skill level

        $leaderboards = $thisDatabaseReader->select($query2, $data, $whereCount1, 0, 2, false, false);
        
        print '<table>    <tr>
        <th>Players of skill level ' . $skill . '</th>
        <th> Wins </th>
        <th> Losses </th>
        <th> Ties </th>
        <th> Goals Scored </th>
        <th> Goals Let </th>
    </tr>';
        foreach ($leaderboards as $board) {
            print'
    <tr>
        <td>' . $board['pmkUser'] . '</td>
            <td>' . $board['fldWins'] . '</td>
                <td>' . $board['fldLosses'] . '</td>
                    <td>' . $board['fldTies'] . '</td>
                        <td>' . $board['fldGoalsScored'] . '</td>
                            <td>' . $board['fldGoalsLet'] . '</td>
        
    </tr>';
        }
        print '</table>';
    }
    ?>
    </div>
        
        
        <?php include "footer.php"; ?>

</body>
</html>
