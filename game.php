<?php
include "top.php";
?>
<?php
set_error_handler('exceptions_error_handler');

function exceptions_error_handler($severity, $message, $filename, $lineno) {
    if (error_reporting() == 0) {
        return;
    }
    if (error_reporting() & $severity) {
        throw new ErrorException($message, 0, $severity, $filename, $lineno);
    }
}

try {
    $ex = $_COOKIE["matchId"];
} catch (Exception $e) {
    print "<h1>You need to create a match up to start a game. </h1>";
    exit;
}
$matchupId = $_COOKIE["matchId"];
$getUsers = 'SELECT fnkUser1, fnkUser2, fldTier,fnkCountriesId FROM `tblMatchUps` WHERE pmkMatchId = ?';
$users = $thisDatabaseReader->select($getUsers, array($matchupId), 1, 0, 0, 0, false, false);
$user1 = $users[0][0];
$user2 = $users[0][1];
$tier = $users[0][2];
$getUserNames = 'SELECT fldFirstName, fldSkill FROM `tblUsers` WHERE pmkUser = ?';
$user1Name = $thisDatabaseReader->select($getUserNames, array($user1), 1, 0, 0, 0, false, false);
$user2Name = $thisDatabaseReader->select($getUserNames, array($user2), 1, 0, 0, 0, false, false);
$getSelectedCountries = 'SELECT * FROM `tblCountries` WHERE pmkCountriesId = ?';
$nations = $thisDatabaseReader->select($getSelectedCountries, array($users[0][3]), 1, 0, 0, 0);

function skillToNum($x) {
    if ($x == "Beginner") {
        return 0;
    }
    if ($x == "Amateur") {
        return 1;
    }
    if ($x == "Semi-Pro") {
        return 2;
    }
    if ($x == "Professional") {
        return 3;
    }
    if ($x == "World-Class") {
        return 4;
    }
    if ($x == "Legendary") {
        return 5;
    }
}

$user1Skill = $user1Name[0][1];
$user2Skill = $user2Name[0][1];
$num1 = skillToNum($user1Skill);
$num2 = skillToNum($user2Skill);
$dif = $num1 - $num2;
$difAdj = floor($dif / 2);
//use $dif to calculate whos team should be better based on if it's negative or positive
$i = 0;
$countries1 = array();
foreach ($nations as $nation) {
    while ($i < 30) {
        if ($nation[$i] == 'true') {
            array_push($countries1, $i);
            $i = $i + 1;
        } else {
            $i = $i + 1;
        }
    }
}
$j = 0;
$countries2 = array();
foreach ($nations as $nation) {
    while ($j < 30) {
        if ($nation[$j] == 'true') {
            array_push($countries2, $j);
            $j = $j + 1;
        } else {
            $j = $j + 1;
        }
    }
}

function findTeam() {
    global $countries1, $countries2, $thisDatabaseReader;
    if (count($countries1) > 1) {
        $country1 = $countries1[(array_rand($countries1, 1))];
    } else {
        $country1 = $countries1[0];
    }
    if (count($countries2) > 1) {
        $country2 = $countries2[(array_rand($countries2, 1))];
    } else {
        $country2 = $countries2[0];
    }
    $selectLeague = 'SELECT pmkLeagueId FROM `tblLeagues` WHERE fnkCountryId = ?';
    $leagues1 = $thisDatabaseReader->select($selectLeague, array($country1), 1, 0, 0, 0, false, false);
    if (count($leagues1) > 1) {
        $league1 = $leagues1[(array_rand($leagues1, 1))];
    } else {
        $league1 = $leagues1[0];
    }
    $leagues2 = $thisDatabaseReader->select($selectLeague, array($country2), 1, 0, 0, 0, false, false);
    if (count($leagues2) > 1) {
        $league2 = $leagues2[(array_rand($leagues2, 1))];
    } else {
        $league2 = $leagues2[0];
    }
    return array($league1, $league2);
}

$teams1 = array();
$teams2 = array();
if ($tier == 'high') {
    //in here have two seperate queries for each user based on the $dif value
    $selectTeam1 = 'SELECT pmkTeamName, fldTeamSkill FROM `tblTeams` WHERE fnkLeagueId = ? AND fldTeamSkill >= 8 - ' . $difAdj . ' AND fldTeamSkill <= 10 - ' . $difAdj . '';
    $selectTeam2 = 'SELECT pmkTeamName, fldTeamSkill FROM `tblTeams` WHERE fnkLeagueId = ? AND fldTeamSkill >= 8 + ' . $difAdj . ' AND fldTeamSkill <= 10 + ' . $difAdj . '';
    $leagueGuys = findTeam();
    $team1 = array();
    $team2 = array();
    for ($i = 0; $i <= 30; $i++) {
        $league1 = $leagueGuys[0];
        $league2 = $leagueGuys[1];
        if (count($team1) == 0) {
            if (count($nations) == 1) {
                $teams1 = $thisDatabaseReader->select($selectTeam1, array($league1[0]), 1, 2, 0, 2, false, false);
            } else {
                $teams1 = $thisDatabaseReader->select($selectTeam1, array($league1[0][0]), 1, 2, 0, 2, false, false);
            }
            if (count($teams1) == 0) {
                $team1 = array();
                $leagueGuys = findTeam();
            } else {
                $team1 = $teams1[(array_rand($teams1))];
            }
        }
        if (count($team2) == 0) {
            if (count($nations) == 1) {
                $teams2 = $thisDatabaseReader->select($selectTeam2, array($league2[0]), 1, 2, 0, 2, false, false);
            } else {
                $teams2 = $thisDatabaseReader->select($selectTeam2, array($league2[0][0]), 1, 2, 0, 2, false, false);
            }
            if (count($teams2) == 0) {
                $team2 = array();
                $leagueGuys = findTeam();
            } else {
                $team2 = $teams2[(array_rand($teams2))];
            }
        }
    }
    if (count($team1) == 0) {
        print '<h3>No teams found for ' . $user1Name[0][0] . ' given your criteria. <a href = "game.php">Refresh the page to try again</a> or <a href = "matchUp.php">go back</a> and change your selection.</h3>';
    }
    if (count($team2) == 0) {
        print '<h3>No teams found for ' . $user2Name[0][0] . ' given your criteria. <a href = "game.php">Refresh the page to try again</a> or <a href = "matchUp.php">go back</a> and change your selection.</h3>';
    }
}
if ($tier == 'med') {
    $selectTeam1 = 'SELECT pmkTeamName FROM `tblTeams` WHERE fnkLeagueId = ? AND fldTeamSkill >= 6 - ' . $difAdj . ' AND fldTeamSkill <= 8 - ' . $difAdj . '';
    $selectTeam2 = 'SELECT pmkTeamName FROM `tblTeams` WHERE fnkLeagueId = ? AND fldTeamSkill >= 6 + ' . $difAdj . ' AND fldTeamSkill <= 8 + ' . $difAdj . '';
    $leagueGuys = findTeam();
    $team1 = array();
    $team2 = array();
    for ($i = 0; $i <= 30; $i++) {
        $league1 = $leagueGuys[0];
        $league2 = $leagueGuys[1];
        if (count($team1) == 0) {
            if (count($nations) == 1) {
                $teams1 = $thisDatabaseReader->select($selectTeam1, array($league1[0]), 1, 2, 0, 2, false, false);
            } else {
                $teams1 = $thisDatabaseReader->select($selectTeam2, array($league1[0][0]), 1, 2, 0, 2, false, false);
            }
            if (count($teams1) == 0) {
                $team1 = array();
                $leagueGuys = findTeam();
            } else {
                $team1 = $teams1[(array_rand($teams1))];
            }
        }
        if (count($team2) == 0) {
            if (count($nations) == 1) {
                $teams2 = $thisDatabaseReader->select($selectTeam1, array($league2[0]), 1, 2, 0, 2, false, false);
            } else {
                $teams2 = $thisDatabaseReader->select($selectTeam2, array($league2[0][0]), 1, 2, 0, 2, false, false);
            }
            if (count($teams2) == 0) {
                $team2 = array();
                $leagueGuys = findTeam();
            } else {
                $team2 = $teams2[(array_rand($teams2))];
            }
        }
    }
    if (count($team1) == 0) {
        print '<h3>No teams found for ' . $user1Name[0][0] . ' given your criteria. <a href = "game.php">Refresh the page to try again</a> or <a href = "matchUp.php">go back</a> and change your selection.</h3>';
    }
    if (count($team2) == 0) {
        print '<h3>No teams found for ' . $user2Name[0][0] . ' given your criteria. <a href = "game.php">Refresh the page to try again</a> or <a href = "matchUp.php">go back</a> and change your selection.</h3>';
    }
}
if ($tier == 'low') {
    $selectTeam1 = 'SELECT pmkTeamName FROM `tblTeams` WHERE fnkLeagueId = ? AND fldTeamSkill <= 4 - ' . $difAdj . ' AND fldTeamSkill >= 0 - ' . $difAdj . '';
    $selectTeam2 = 'SELECT pmkTeamName FROM `tblTeams` WHERE fnkLeagueId = ? AND fldTeamSkill <= 4 + ' . $difAdj . ' AND fldTeamSkill >= 0 + ' . $difAdj . '';
    $leagueGuys = findTeam();
    $team1 = array();
    $team2 = array();
    for ($i = 0; $i <= 30; $i++) {
        $league1 = $leagueGuys[0];
        $league2 = $leagueGuys[1];
        if (count($team1) == 0) {
            if (count($nations) == 1) {
                $teams1 = $thisDatabaseReader->select($selectTeam1, array($league1[0]), 1, 2, 0, 2, false, false);
            } else {
                $teams1 = $thisDatabaseReader->select($selectTeam2, array($league1[0][0]), 1, 2, 0, 2, false, false);
            }
            if (count($teams1) == 0) {
                $team1 = array();
                $leagueGuys = findTeam();
            } else {
                $team1 = $teams1[(array_rand($teams1))];
            }
        }
        if (count($team2) == 0) {
            if (count($nations) == 1) {
                $teams2 = $thisDatabaseReader->select($selectTeam2, array($league2[0]), 1, 2, 0, 2, false, false);
            } else {
                $teams2 = $thisDatabaseReader->select($selectTeam2, array($league2[0][0]), 1, 2, 0, 2, false, false);
            }
            if (count($teams2) == 0) {
                $team2 = array();
                $leagueGuys = findTeam();
            } else {
                $team2 = $teams2[(array_rand($teams2))];
            }
        }
    }
    if (count($team1) == 0) {
        print '<h3>No teams found for ' . $user1Name[0][0] . ' given your criteria. <a href = "game.php">Refresh the page to try again</a> or <a href = "matchUp.php">go back</a> and change your selection.</h3>';
    }
    if (count($team2) == 0) {
        print '<h3>No teams found for ' . $user2Name[0][0] . ' given your criteria. <a href = "game.php">Refresh the page to try again</a> or <a href = "matchUp.php">go back</a> and change your selection.</h3>';
    }
}
$user1Score = '';
$user2Score = '';
if (isset($_POST["submit"])) {
    $user1Score = htmlentities($_POST["user1Score"], ENT_QUOTES, "UTF-8");
    $user2Score = htmlentities($_POST["user2Score"], ENT_QUOTES, "UTF-8");
    //if ($user1Score !="" && $user2Score !=""){
    $data = array($user1, $user2, $user1Name[0][1], $user2Name[0][1], $tier, $user1Score, $user2Score, $_COOKIE["scoreId"]);
    $result = "UPDATE `tblScores` SET `fnkUser1`= ?,`fnkUser2`= ?,`fnkUser1Skill`= ?,`fnkUser2Skill`= ?,`fnkTier`= ?, `fldUser1Score`= ?,`fldUser2Score`= ? WHERE pmkScoreId = ?";
    $saveResults = $thisDatabaseWriter->update($result, $data, 1, 0, 0, 0, false, false);
    $getRecord = 'SELECT `fldWins`, `fldLosses`,`fldTies`,`fldGoalsScored`,`fldGoalsLet` FROM `tblUsers` WHERE pmkUser = ?';
    $recordUser1 = $thisDatabaseReader->select($getRecord, array($user1), 1, 0, 0, 0, false, false);
    $recordUser2 = $thisDatabaseReader->select($getRecord, array($user2), 1, 0, 0, 0, false, false);
    $user1Wins = $recordUser1[0][0];
    $user2Wins = $recordUser2[0][0];
    $user1Losses = $recordUser1[0][1];
    $user2Losses = $recordUser2[0][1];
    $user1Ties = $recordUser1[0][2];
    $user2Ties = $recordUser2[0][2];
    $user1GoalsFor = $recordUser1[0][3];
    $user2GoalsFor = $recordUser2[0][3];
    $user1GoalsLet = $recordUser1[0][4];
    $user2GoalsLet = $recordUser2[0][4];
    if ($user1Score > $user2Score) {
        $user1Wins = $user1Wins + 1;
        $user2Losses = $user2Losses + 1;
        $user1GoalsFor = $user1GoalsFor + $user1Score;
        $user2GoalsFor = $user2GoalsFor + $user2Score;
        $user1GoalsLet = $user1GoalsLet + $user2Score;
        $user2GoalsLet = $user2GoalsLet + $user1Score;
        $updateRecords = 'UPDATE `tblUsers` SET fldWins = ?,`fldLosses`= ?,`fldGoalsScored`= ?,`fldGoalsLet`= ? WHERE pmkUser = ?';
        $updated1 = $thisDatabaseWriter->update($updateRecords, array($user1Wins, $user1Losses, $user1GoalsFor, $user1GoalsLet, $user1), 1, 0, 0, 0, false, false);
        $updated2 = $thisDatabaseWriter->update($updateRecords, array($user2Wins, $user2Losses, $user2GoalsFor, $user2GoalsLet, $user2), 1, 0, 0, 0, false, false);
        if ($user1Wins % 3 == 0) {
            if ($user1Skill == 'Beginner') {
                $promote = 'UPDATE `tblUsers` SET `fldSkill`= "Amateur" WHERE pmkUser = ?';
                $promoteUser = $thisDatabaseWriter->update($promote, array($user1), 1, 0, 2, 0, false, false);
            }
            if ($user1Skill == 'Amateur') {
                $promote = 'UPDATE `tblUsers` SET `fldSkill`= "Semi-Pro" WHERE pmkUser = ?';
                $promoteUser = $thisDatabaseWriter->update($promote, array($user1), 1, 0, 2, 0, false, false);
            }
            if ($user1Skill == 'Semi-Pro') {
                $promote = 'UPDATE `tblUsers` SET `fldSkill`= "Professional" WHERE pmkUser = ?';
                $promoteUser = $thisDatabaseWriter->update($promote, array($user1), 1, 0, 2, 0, false, false);
            }
            if ($user1Skill == 'Professional') {
                $promote = 'UPDATE `tblUsers` SET `fldSkill`= "World-Class" WHERE pmkUser = ?';
                $promoteUser = $thisDatabaseWriter->update($promote, array($user1), 1, 0, 2, 0, false, false);
            }
            if ($user1Skill == 'World-Class') {
                $promote = 'UPDATE `tblUsers` SET `fldSkill`= "Legendary" WHERE pmkUser = ?';
                $promoteUser = $thisDatabaseWriter->update($promote, array($user1), 1, 0, 2, 0, false, false);
            }
            if ($user1Skill == 'Legendary') {
                print '<h2>Nice job, ' . $user1Name[0][0] . '! You would be promoted but you are already the highest skill level.</h2>';
            } else {
                $getSkill = 'SELECT fldSkill FROM `tblUsers` WHERE pmkUser = ?';
                $skill = $thisDatabaseReader->select($getSkill, array($user1), 1, 0, 0, 0, false, false);
                print '<h2>Congratulations ' . $user1Name[0][0] . ', you have been promoted up a skill level to ' . $skill[0][0] . '!</h2>';
            }
        }
        if ($user2Losses % 3 == 0) {
            if ($user2Skill == 'Legendary') {
                $demote = 'UPDATE `tblUsers` SET `fldSkill`= "World-Class" WHERE pmkUser = ?';
                $demoteUser = $thisDatabaseWriter->update($demote, array($user2), 1, 0, 2, 0, false, false);
            }
            if ($user2Skill == 'World-Class') {
                $demote = 'UPDATE `tblUsers` SET `fldSkill`= "Professional" WHERE pmkUser = ?';
                $demoteUser = $thisDatabaseWriter->update($demote, array($user2), 1, 0, 2, 0, false, false);
            }
            if ($user2Skill == 'Professional') {
                $demote = 'UPDATE `tblUsers` SET `fldSkill`= "Semi-Pro" WHERE pmkUser = ?';
                $demoteUser = $thisDatabaseWriter->update($demote, array($user2), 1, 0, 2, 0, false, false);
            }
            if ($user2Skill == 'Semi-Pro') {
                $demote = 'UPDATE `tblUsers` SET `fldSkill`= "Amateur" WHERE pmkUser = ?';
                $demoteUser = $thisDatabaseWriter->update($demote, array($user2), 1, 0, 2, 0, false, false);
            }
            if ($user2Skill == 'Amateur') {
                $demote = 'UPDATE `tblUsers` SET `fldSkill`= "Beginner" WHERE pmkUser = ?';
                $demoteUser = $thisDatabaseWriter->update($demote, array($user2), 1, 0, 2, 0, false, false);
            }
            if ($user2Skill == 'Beginner') {
                print '<h2>Sorry ' . $user2Name[0][0] . ', you would be demoted but you are already the lowest skill level.</h2>';
            } else {
                $getSkill = 'SELECT fldSkill FROM `tblUsers` WHERE pmkUser = ?';
                $skill = $thisDatabaseReader->select($getSkill, array($user2), 1, 0, 0, 0, false, false);
                print '<h2>Sorry ' . $user2Name[0][0] . ', you have been demoted down a skill level to ' . $skill[0][0] . '.</h2>';
            }
        }
    }
    if ($user1Score < $user2Score) {
        $user1Losses = $user1Losses + 1;
        $user2Wins = $user2Wins + 1;
        $user1GoalsFor = $user1GoalsFor + $user1Score;
        $user2GoalsFor = $user2GoalsFor + $user2Score;
        $user1GoalsLet = $user1GoalsLet + $user2Score;
        $user2GoalsLet = $user2GoalsLet + $user1Score;
        $updateRecords = 'UPDATE `tblUsers` SET fldWins = ?,`fldLosses`= ?,`fldGoalsScored`= ?,`fldGoalsLet`= ? WHERE pmkUser = ?';
        $updated1 = $thisDatabaseWriter->update($updateRecords, array($user1Wins, $user1Losses, $user1GoalsFor, $user1GoalsLet, $user1), 1, 0, 0, 0, false, false);
        $updated2 = $thisDatabaseWriter->update($updateRecords, array($user2Wins, $user2Losses, $user2GoalsFor, $user2GoalsLet, $user2), 1, 0, 0, 0, false, false);
        if ($user2Wins % 3 == 0) {
            if ($user2Skill == 'Beginner') {
                $promote = 'UPDATE `tblUsers` SET `fldSkill`= "Amateur" WHERE pmkUser = ?';
                $promoteUser = $thisDatabaseWriter->update($promote, array($user2), 1, 0, 2, 0, false, false);
            }
            if ($user2Skill == 'Amateur') {
                $promote = 'UPDATE `tblUsers` SET `fldSkill`= "Semi-Pro" WHERE pmkUser = ?';
                $promoteUser = $thisDatabaseWriter->update($promote, array($user2), 1, 0, 2, 0, false, false);
            }
            if ($user2Skill == 'Semi-Pro') {
                $promote = 'UPDATE `tblUsers` SET `fldSkill`= "Professional" WHERE pmkUser = ?';
                $promoteUser = $thisDatabaseWriter->update($promote, array($user2), 1, 0, 2, 0, false, false);
            }
            if ($user2Skill == 'Professional') {
                $promote = 'UPDATE `tblUsers` SET `fldSkill`= "World-Class" WHERE pmkUser = ?';
                $promoteUser = $thisDatabaseWriter->update($promote, array($user2), 1, 0, 2, 0, false, false);
            }
            if ($user2Skill == 'World-Class') {
                $promote = 'UPDATE `tblUsers` SET `fldSkill`= "Legendary" WHERE pmkUser = ?';
                $promoteUser = $thisDatabaseWriter->update($promote, array($user2), 1, 0, 2, 0, false, false);
            }
            if ($user2Skill == 'Legendary') {
                print '<h2>Nice job, ' . $user2Name[0][0] . '! You would be promoted but you are already the highest skill level.</h2>';
            } else {
                $getSkill = 'SELECT fldSkill FROM `tblUsers` WHERE pmkUser = ?';
                $skill = $thisDatabaseReader->select($getSkill, array($user2), 1, 0, 0, 0, false, false);
                print '<h2>Congratulations ' . $user2Name[0][0] . ', you have been promoted up a skill level to ' . $skill[0][0] . '!</h2>';
            }
        }
        if ($user1Losses % 3 == 0) {
            if ($user1Skill == 'Legendary') {
                $demote = 'UPDATE `tblUsers` SET `fldSkill`= "World-Class" WHERE pmkUser = ?';
                $demoteUser = $thisDatabaseWriter->update($demote, array($user1), 1, 0, 2, 0, false, false);
            }
            if ($user1Skill == 'World-Class') {
                $demote = 'UPDATE `tblUsers` SET `fldSkill`= "Professional" WHERE pmkUser = ?';
                $demoteUser = $thisDatabaseWriter->update($demote, array($user1), 1, 0, 2, 0, false, false);
            }
            if ($user1Skill == 'Professional') {
                $demote = 'UPDATE `tblUsers` SET `fldSkill`= "Semi-Pro" WHERE pmkUser = ?';
                $demoteUser = $thisDatabaseWriter->update($demote, array($user1), 1, 0, 2, 0, false, false);
            }
            if ($user1Skill == 'Semi-Pro') {
                $demote = 'UPDATE `tblUsers` SET `fldSkill`= "Amateur" WHERE pmkUser = ?';
                $demoteUser = $thisDatabaseWriter->update($demote, array($user1), 1, 0, 2, 0, false, false);
            }
            if ($user1Skill == 'Amateur') {
                $demote = 'UPDATE `tblUsers` SET `fldSkill`= "Beginner" WHERE pmkUser = ?';
                $demoteUser = $thisDatabaseWriter->update($demote, array($user1), 1, 0, 2, 0, false, false);
            }
            if ($user1Skill == 'Beginner') {
                print '<h2>Sorry ' . $user1Name[0][0] . ', you would be demoted but you are already the lowest skill level.</h2>';
            } else {
                $getSkill = 'SELECT fldSkill FROM `tblUsers` WHERE pmkUser = ?';
                $skill = $thisDatabaseReader->select($getSkill, array($user1), 1, 0, 0, 0, false, false);
                print '<h2>Sorry ' . $user1Name[0][0] . ', you have been demoted down a skill level to ' . $skill[0][0] . '.</h2>';
            }
        }
    }
    if ($user1Score == $user2Score) {
        $user1Ties = $user1Ties + 1;
        $user2Ties = $user2Ties + 1;
        $user1GoalsFor = $user1GoalsFor + $user1Score;
        $user2GoalsFor = $user2GoalsFor + $user2Score;
        $user1GoalsLet = $user1GoalsLet + $user2Score;
        $user2GoalsLet = $user2GoalsLet + $user1Score;
        $updateRecords = 'UPDATE `tblUsers` SET fldTies = ?,`fldGoalsScored`= ?,`fldGoalsLet`= ? WHERE pmkUser = ?';
        $updated1 = $thisDatabaseWriter->update($updateRecords, array($user1Ties, $user1GoalsFor, $user1GoalsLet, $user1), 1, 0, 0, 0, false, false);
        $updated2 = $thisDatabaseWriter->update($updateRecords, array($user2Ties, $user2GoalsFor, $user2GoalsLet, $user2), 1, 0, 0, 0, false, false);
    }
    ?>
    <fieldset class = "restart">
        <h2>Play another game?</h2>
        <form method = "post" action = "matchUp.php">
            <input type = "submit" class = "btnSubmit" name = "newGame" value = "New Users" tabindex= "900" class = "button">
        </form>
        <form method = "post" action = "game.php">
            <input type = "submit" class = "btnSubmit" name = "restart" value = "Same Users" tabindex = "900" class = "button">
        </form>
    </fieldset>
    <fieldset class = "restart">
        <h2>Check your stats</h2>
        <form method = "post" action = "score.php">
            <input type = "submit" class = "btnSubmit" name = "scores" value = "View Scores" tabindex= "900" class = "button">
        </form>
    </fieldset>
    <?php
} else {
    if (count($teams1) != 0 and count($teams2) != 0) {
        $getLeague = 'SELECT fnkLeagueId FROM tblTeams WHERE pmkTeamName = ?';
        $leagueId1 = $thisDatabaseReader->select($getLeague, array($team1[0]), 1, 0, 0, 0, false, false);
        $leagueId2 = $thisDatabaseReader->select($getLeague, array($team2[0]), 1, 0, 0, 0, false, false);
        $getTeamInfo = 'SELECT fldLeague, fldCountryName FROM tblLeagues WHERE pmkLeagueId = ?';
        $league1 = $thisDatabaseReader->select($getTeamInfo, array($leagueId1[0][0]), 1, 0, 0, 0, false, false);
        $league2 = $thisDatabaseReader->select($getTeamInfo, array($leagueId2[0][0]), 1, 0, 0, 0, false, false);
        ?>
<form method = "post">
        <fieldset class= "guy">
            <legend><?php print_r($user1Name[0][0]) ?></legend>
            <h3>Country: <?php print_r($league1[0][1]) ?></h3>
            <h3>League: <?php print_r($league1[0][0]) ?></h3>
            <h3>Team: <?php print_r($team1[0]) ?></h3>
            
            <label>Score: </label>
             <input type = "text"
                           name = "user1Score" 
                           value = "<?php if (isset($_POST["user1Score"])) print $_POST[$user1Score] ?>" 
                           maxlength = "2" 
                           placeholder = "Enter <?php print $user1Name[0][0] ?>'s score">
        </fieldset>
        <fieldset class = "guy">
            <h1>VS.</h1>
        </fieldset>
        <fieldset class= "guy">
            <legend><?php print_r($user2Name[0][0]) ?></legend>
            <h3>Country: <?php print_r($league2[0][1]) ?></h3>
            <h3>League: <?php print_r($league2[0][0]) ?></h3>
            <h3>Team: <?php print_r($team2[0]) ?></h3>
            
            <label>Score: </label>
            <input type = "text"
                           name = "user2Score" 
                           value = "<?php if (isset($_POST["user2Score"])) print $_POST[$user2Score] ?>" 
                           maxlength = "2" 
                           placeholder = "Enter <?php print $user2Name[0][0] ?>'s score">
        </fieldset>
            
                
                
                   
                    
                
        
        <?php
        $insertTeams = "INSERT into tblScores SET
        fnkMatchupId = '" . $matchupId . "',
        fldUser1Team = '" . $team1[0] . "',
        fldUser2Team = '" . $team2[0] . "' ;";

        $pushTeams = $thisDatabaseWriter->insert($insertTeams, '', 0, 0, 6, 0, false, false);
        $getScoreId = 'SELECT MAX(pmkScoreId) FROM tblScores WHERE fnkMatchupId = ?';
        $scoreId = $thisDatabaseReader->select($getScoreId, array($matchupId), 1, 0, 0, 0, false, false);
        $scoreId = $scoreId[0][0];
        setcookie("scoreId", $scoreId);
        $getTeams = 'SELECT fldUser1Team, fldUser2Team FROM tblScores WHERE pmkScoreId = ?';
        $teams = $thisDatabaseReader->select($getTeams, array($scoreId), 1, 0, 0, 0, false, false);
        ?>
        <fieldset class = "buttons">
            <legend></legend>
            <input type = "submit" name = "submit" value = "Submit Score" tabindex= "900" class =  "btnSubmit">
        </fieldset>
        </form>
        <?php
    }
}
?>
<?php include "footer.php"; ?>
</body>
</html>