<?php
include "top.php";
?>
<h2> Welcome to FIFA Match Maker! </h2>
<p>Our site is for competitive FIFA players that want to keep track of their games with friends. Our algorithms will adjust each players teams based on their skill levels, and skill 
levels can change based on how well you play. This allows for the most fair games, keeping every game close. Your stats, and your friends stats, will be tracked and can be viewed at anytime.
Enjoy!</p>
<p>Want to learn more about us? Click <a href="about.php">here.</a></p>
<h3>If you haven't created an account yet, create one <a href="createAccount.php">here.</a> </h3>
<p>Your account will allow you to create a match up with other users. The data for your account will be kept in tblUsers.</p>
<h3>Want to play against a friend, click <a href="matchUp.php">here!</a> </h3>
<p>You can select any countries you'd like your teams to be from, and how good you want your teams to be. The match up you create will be stored in tblMatchUps, 
    with two foreign keys from tblUsers and a foreign key from tblCountries. This will send you to the game page, which stores your score in tblScores and has a foreign key from tblMatchUps</p>
<h3>Check out the Leader-Board <a href="score.php">here!</a> </h3>
<p>Check out the most recent games played and the stats. Also check out the ranks of users, or even search for a user to check out their stats. This displays information from tblUsers and tblScores.</p>
<?php include "footer.php"; ?>

</body>
</html>